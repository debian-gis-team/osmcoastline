Source: osmcoastline
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               libbz2-dev,
               libexpat1-dev,
               libgdal-dev,
               libgeos++-dev,
               libosmium2-dev (>= 2.16.0),
               libgdalcpp-dev,
               pandoc,
               spatialite-bin,
               sqlite3,
               zlib1g-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/osmcoastline
Vcs-Git: https://salsa.debian.org/debian-gis-team/osmcoastline.git
Homepage: https://osmcode.org/osmcoastline/
Rules-Requires-Root: no

Package: osmcoastline
Architecture: any
Section: utils
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Extract coastline data from OpenStreetMap planet file
 OSMCoastline extracts the coastline from an OSM planet file and assembles all
 the pieces into polygons for use in map renderers etc.
 .
 OSMCoastline relies on the Osmium library for its OpenStreetMap data handling.
 .
 The Osmium library has extensive support for all types of OSM entities: nodes,
 ways, relations, and changesets. It allows reading from and writing to OSM
 files in XML and PBF formats, including change files and full history files.
 Osmium can store OSM data in memory and on disk in various formats and using
 various indexes. Its easy to use handler interface allows you to quickly write
 data filtering and conversion functions. Osmium can create WKT, WKB, OGR, GEOS
 and GeoJSON geometries for easy conversion into many GIS formats and it can
 assemble multipolygons from ways and relations.
 .
 This package contains the OSMCoastline utilities.
